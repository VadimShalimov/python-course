import sys
import pygame

import random
import functools

pygame.init()

left = [pygame.image.load(f"l{i}.png") for i in range(1, 6)]
right = [pygame.image.load(f"r{i}.png") for i in range(1, 6)]

window = pygame.display.set_mode((640, 480))
rect = pygame.Rect(40, 40, 120, 120)
clock = pygame.time.Clock()
i = 0
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                rect.move_ip(-40, 0)
            elif event.key == pygame.K_RIGHT:
                rect.move_ip(40, 0)
            elif event.key == pygame.K_UP:
                rect.move_ip(0, -40)
            elif event.key == pygame.K_DOWN:
                rect.move_ip(0, 40)

    window.fill((0,0,0))
    pygame.draw.rect(window, (255, 0, 0), rect, 0)

    window.blit(left[i // 12], (20, 20))
    window.blit(right[i // 12], (100, 20))
    i += 1
    if i == 60:
        i = 0

    pygame.display.flip()
    clock.tick(60)