.. _SimpleServer:

Простой http сервер.
============================================

Теория
-------
Любой online-сервис начинается с того, что необходимо приложение, которое
способно принимать и обрабатывать http-запросы.

В ``python`` есть встроенный модуль, который полвзоляет создавать простой http-сервер.

.. code::

  from http.server import BaseHTTPRequestHandler
  from http.server import HTTPServer

  def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()


Если запустить функцию ``run()``, то будет запущен http-сервер, и если в браузере
Вы введете ``127.0.0.1:8000``, то Вам вернутся ответ, но ответ будет содержать ошибку,
потому что сервер не знает как обрабатывать запросы.

Для того что бы исправить эту ошибку трубется в обработчике(``BaseHTTPRequestHandler``)
реализовать метод по обработке GET-запросов:

.. code::

  class HttpGetHandler(BaseHTTPRequestHandler):
      """Обработчик с реализованным методом do_GET."""

      def do_GET(self):
          self.send_response(200)
          self.send_header("Content-type", "text/html")
          self.end_headers()
          self.wfile.write('<html><head><meta charset="utf-8">'.encode())
          self.wfile.write('<title>Простой HTTP-сервер.</title></head>'.encode())
          self.wfile.write('<body>Был получен GET-запрос.</body></html>'.encode())


Теперь, если запустить сервер с новым обработчиком ``run(handler_class=HttpGetHandler)``,
то при переходе по адресу ``127.0.0.1:8000`` можно будет увидеть ответ на запрос.

.. important::

  Обратите внимание, что при формировании ответа обработчика был использован метод
  ``encode()`` у строк. Сделано этого потому что в теле ответа могут быть
  использованы только байтовые строки.


Задания
-------

#. Ознакомьтесь с описанием протокола `HTTP <https://ru.wikipedia.org/wiki/HTTP>`_.
#. Дополните обработчик запросов таким образом, что бы он мог обрабатывать запросы по методам POST, PUT и HEAD.
   Для проверки реализации используйте библиотку `requests <https://python-course.readthedocs.io/projects/year1/en/latest/lessons/17-requests.html>`_.

