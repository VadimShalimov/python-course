HANGMANPICS = ['''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']


words = ('ant baboon badger bat bear beaver camel cat clam cobra cougar '
         'coyote crow deer dog donkey duck eagle ferret fox frog goat '
         'goose hawk lion lizard llama mole monkey moose mouse mule newt '
         'otter owl panda parrot pigeon python rabbit ram rat raven '
         'rhino salmon seal shark sheep skunk sloth snake spider '
         'stork swan tiger toad trout turkey turtle weasel whale wolf '
         'wombat zebra ').split()

alpth = 'a b c d e f g h i j k l m n o p q r s t u v w x y z'.split()
import random
import string

def main():
    word = random.choice(words)
    n = 0
    alpth_game = ''
    for i in alpth:
        alpth_game+=i
    word_game = '.'*len(word)
    print (word)
    while True:
        if n < len (HANGMANPICS):
            print (alpth_game)
            print (word_game)
            letter = input ('Введите букву: ')
            alpth_game = alpth_game.replace (letter, '.')
            if letter in word:
                for i in range(len(word)):
                    if word[i] == letter:
                           word_game = word_game[:i] + letter + word_game[i+1:]
            else:
                print('Такой буквы нет')
                print (HANGMANPICS[n])
                n+=1
        else:
            print ('Вы проиграли!')
            break
if __name__ == '__main__':
    main()
