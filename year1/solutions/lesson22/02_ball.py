import math
import sys

import pygame
from pygame.color import THECOLORS

pygame.mixer.init(22050, -16, 2, 64)
pygame.init()

WIDTH = 1280
HEIGHT = 800

screen = pygame.display.set_mode((WIDTH, HEIGHT))

circle_center = (WIDTH // 2, HEIGHT // 2)
circle_radius = 50
circle_color = THECOLORS['purple2']

SPEED = 20

current_angle = -10


def get_move(angle):
    angle = angle / 180. * math.pi
    return (
        int(SPEED * math.cos(angle)),
        int(SPEED * math.sin(angle)))


def move_circle(center, move):
    return (
        center[0] + move[0],
        center[1] + move[1])


def get_angle(center):
    if (center[0] + circle_radius >= WIDTH or
            center[0] - circle_radius <= 0):
        return 180 - current_angle
    if (center[1] - circle_radius <= 0 or
            center[1] + circle_radius >= HEIGHT):
        return -current_angle
    return current_angle


move = get_move(current_angle)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    screen.fill(THECOLORS['black'])

    current_angle = get_angle(circle_center)
    move = get_move(current_angle)
    circle_center = move_circle(circle_center, move)

    pygame.draw.circle(screen, circle_color, circle_center, circle_radius)

    pygame.display.flip()
    pygame.time.wait(33)
