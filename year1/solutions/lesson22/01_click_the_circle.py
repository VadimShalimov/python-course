import math
import random
import sys

import pygame
from pygame.color import THECOLORS

pygame.mixer.init(22050, -16, 2, 64)
pygame.init()

WIDTH = 640
HEIGHT = 480

screen = pygame.display.set_mode((WIDTH, HEIGHT))

color = THECOLORS['purple1']

circle_radius = 50


def distance(p1, p2):
    # TODO(3.3): Реализуйте функцию, которая вычисляет
    #  расстояние между точками p1 и p2.
    x1, y1 = p1
    x2, y2 = p2
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def point_in_circle(p, circle_center, circle_radius):
    # TODO(3.2): Реализуйте функцию, которая возвращает True,
    #  если точка находится внутри круга.
    #  Для полноценной работы функции вам понадобится
    #  реализовать функцию distance, но написать определение
    #  принадлежности точки кругу вы можете уже сейчас.
    return distance(circle_center, p) < circle_radius


def place_circle():
    # TODO(1.1): Функция должна вернуть кортеж, состоящий из
    #   координат центра круга: (x, y)
    # return (WIDTH // 2, HEIGHT // 2)
    return (
        random.randint(0 + circle_radius, WIDTH - circle_radius),
        random.randint(0 + circle_radius, HEIGHT - circle_radius))

# TODO(1.2): инициализируйте центр круга
circle_center = place_circle()

# TODO(5.1): Загрузите звук выстрела.
#  Воспользуйтесь конструктором
#  pygame.mixer.Sound(file_path: str) -> Sound
shot = pygame.mixer.Sound('shot.wav')

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        # TODO(2.1): Обработайте событие нажатия кнопки мыши pygame.MOUSEBUTTONDOWN
        # TODO(2.2): При нажатии кнопки мыши смените цвет круга.
        # TODO(3.1): Доработайте приложение так, чтобы круг менял цвет только если
        #  вы кликаете по нему. Для этого вам понадобирся реализовать функции
        #  point_in_circle и distance.
        # TODO(4.1): доработайте приложение так, чтобы при
        #  клике по кругу он появлялся в случайной точке
        #  экрана.
        # TODO(5.2): Воспроизведите звук выстрела в случае попадания по кругу.
        #  Используйте метод play() объекта класса Sound.
        # TODO(6.1): Реализуйте подсчет очков. Если игрок попал в круг за секунду,
        #  то он получает 100 очков. За 2 секунды - 50. Дольше - 1.
        if event.type == pygame.MOUSEBUTTONDOWN:
            if point_in_circle(event.pos, circle_center, circle_radius):
                # color = THECOLORS['red']
                shot.play()
                circle_center = place_circle()

    screen.fill(THECOLORS['black'])

    # TODO(1.3) Нарисуйте круг выбранного цвета.
    pygame.draw.circle(screen, color, circle_center, circle_radius)

    pygame.display.flip()
    pygame.time.wait(33)