import random
import sys
import pygame
from pygame.color import THECOLORS

# TODO:
#  * Выход
#  * Выход только со сбором всех ключей

# # - стена
# @ - игрок
# K - ключ
# X - выход
MAZE_MAP = [
    '#######@#####',
    '#  K#     #K#',
    '# ### # # # #',
    '# #   # #   #',
    '# # # #######',
    '# # #       #',
    '# # ####### #',
    '# #     #K# #',
    '# ##### # # #',
    '#   #   # # #',
    '### # ### # #',
    '#     #     #',
    '#X###########',
]

BLOCK_SIDE = 64
CELL_SIDE = 64
WIDTH = len(MAZE_MAP[0])
HEIGHT = len(MAZE_MAP)

SCREEN_WIDTH = WIDTH * BLOCK_SIDE
SCREEN_HEIGHT = HEIGHT * BLOCK_SIDE

wall_textures = []
for i in range(0, 16):
    wall_textures.append(pygame.image.load(f'img/wall_64x64_{i}.png'))

key_texture = pygame.image.load(f'img/key.png')
door_textures = []
for i in range(4):
    door_textures.append(pygame.image.load(f'img/door{i}.png'))
player_texture = pygame.image.load(f'img/player.png')


pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
font = pygame.font.SysFont('arial', 60)
text = font.render('You Win!', True, THECOLORS['green'])


class WallCell:
    def __init__(self, x, y, texture):
        self.x = x
        self.y = y
        self.texture = texture

    def draw(self):
        screen.blit(self.texture, (self.x, self.y))


class Key:
    def __init__(self, x, y, cell, texture):
        self.x = x
        self.y = y
        self.cell = cell
        self.texture = texture
        self.is_taken = False

    def draw(self):
        if not self.is_taken:
            screen.blit(self.texture, (self.x, self.y))


class Exit:
    def __init__(self, x, y, cell, textures):
        self.x = x
        self.y = y
        self.cell = cell
        self.keys_collected = 0
        self.textures = textures
        self.is_open = False

    def draw(self):
        screen.blit(self.textures[self.keys_collected], (self.x, self.y))


class Maze:
    def __init__(self):
        self.walls = []
        self.keys = []
        self.exit = None
        self.player = None
        for cell_y in range(len(MAZE_MAP)):
            current_row = []
            for cell_x in range(len(MAZE_MAP[cell_y])):
                cell = MAZE_MAP[cell_y][cell_x]
                x = CELL_SIDE * cell_x
                y = CELL_SIDE * cell_y
                if cell == '#':
                    texture = random.choice(wall_textures)
                    current_row.append(WallCell(x, y, texture))
                elif cell == '@':
                    self.player = Player(x, y)
                elif cell == 'K':
                    self.keys.append(Key(x, y, (cell_x, cell_y), key_texture))
                elif cell == 'X':
                    self.exit = Exit(x, y, (cell_x, cell_y), door_textures)
            self.walls.append(current_row)

    def move_player(self):
        self.set_player_direction(self.player.direction)
        self.player.move()
        cell = self.get_player_cell()
        for key in self.keys:
            if key.cell == cell:
                key.is_taken = True

        keys_collected = 0
        for key in self.keys:
            if key.is_taken:
                keys_collected += 1
        self.exit.keys_collected = keys_collected

    def set_player_direction(self, direction):
        if self.player_can_move(direction):
            self.player.direction = direction
        else:
            self.player.direction = Direction.NONE

    def player_can_move(self, direction):
        pcx, pcy = self.get_player_cell()
        if ((direction == Direction.UP and pcy == 0) or
                (direction == Direction.DOWN and pcy == HEIGHT - 1) or
                (direction == Direction.LEFT and pcx == 0) or
                (direction == Direction.RIGHT and pcx == WIDTH - 1)):
            return False
        if ((direction == Direction.UP and MAZE_MAP[pcy - 1][pcx] == '#') or
                (direction == Direction.DOWN and MAZE_MAP[pcy + 1][pcx] == '#') or
                (direction == Direction.LEFT and MAZE_MAP[pcy][pcx - 1] == '#') or
                (direction == Direction.RIGHT and MAZE_MAP[pcy][pcx + 1] == '#')):
            return False
        return True

    def draw(self):
        for row in self.walls:
            for cell in row:
                cell.draw()
        for key in self.keys:
            key.draw()
        self.exit.draw()
        self.player.draw()

    def win(self):
        return (self.get_player_cell() == self.exit.cell)# and
                #self.exit.keys_collected == len(self.keys))

    def get_player_cell(self):
        return (self.player.x // CELL_SIDE, self.player.y // CELL_SIDE)


class Direction:
    NONE = 0
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4


class Player:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.texture = player_texture
        self.speed = CELL_SIDE
        self.direction = Direction.NONE

    def draw(self):
        screen.blit(self.texture, (self.x, self.y))

    def move(self):
        if self.direction == Direction.NONE:
            return
        if self.direction == Direction.UP:
            self.y -= self.speed
        elif self.direction == Direction.DOWN:
            self.y += self.speed
        elif self.direction == Direction.LEFT:
            self.x -= self.speed
        elif self.direction == Direction.RIGHT:
            self.x += self.speed


def start_level():
    maze = Maze()

    while True:
        if maze.win():
            return

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    maze.set_player_direction(Direction.UP)
                elif event.key == pygame.K_DOWN:
                    maze.set_player_direction(Direction.DOWN)
                elif event.key == pygame.K_LEFT:
                    maze.set_player_direction(Direction.LEFT)
                elif event.key == pygame.K_RIGHT:
                    maze.set_player_direction(Direction.RIGHT)
            elif event.type == pygame.KEYUP:
                maze.player.direction = Direction.NONE

        maze.move_player()

        screen.fill((74, 117, 44))
        maze.draw()

        pygame.display.flip()
        pygame.time.wait(66)


def show_win_message():
    screen.blit(text, (CELL_SIDE * 2, SCREEN_HEIGHT - CELL_SIDE - 60))
    pygame.display.flip()
    pygame.time.wait(1000)


while True:
    start_level()
    show_win_message()
