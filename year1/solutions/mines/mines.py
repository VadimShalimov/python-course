import pygame
import random

from pygame import gfxdraw


CELL_SIDE = 30

WIDTH = 18
HEIGHT = 14
MENU_HEIGHT = 60

SCREEN_WIDTH = CELL_SIDE * WIDTH
SCREEN_HEIGHT = CELL_SIDE * HEIGHT + MENU_HEIGHT

pygame.init()
SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Mines")


class GameState:
    progress = 1
    win = 2
    lose = 3


class Ui:
    closed_colors = ((170, 215, 81), (162, 209, 73))
    open_colors = ((229, 194, 159), (215, 184, 153))
    selected_colors = ((191, 225, 125), (185, 221, 119))

    # Цвет открытой ячейки с миной
    mined_cell_color = (219, 50, 54)
    mine_radius = 8
    mine_color = (0, 0, 0, 200)

    # Цвета счетчиков мин
    mine_count_colors = (
        # Синий
        (25, 118, 210),
        # Зеленый
        (56, 142, 60),
        # Красный
        (211, 47, 47),
        # Сиреневый
        (123, 31, 162)
    )

    flag_color = (230, 51, 7)

    menu_font_color = (246, 248, 244)

    def __init__(self):
        self.field = None
        self.mine_img = self.create_mine_img()
        self.menu_font = pygame.font.SysFont('consolas', 25)
        self.game_state = GameState.progress

    def create_mine_img(self):
        mine_img = pygame.Surface((CELL_SIDE, CELL_SIDE), pygame.SRCALPHA)
        pygame.gfxdraw.aacircle(
            mine_img, CELL_SIDE // 2, CELL_SIDE // 2,
            self.mine_radius, self.mine_color)
        pygame.gfxdraw.filled_circle(
            mine_img, CELL_SIDE // 2, CELL_SIDE // 2,
            self.mine_radius, self.mine_color)
        return mine_img

    def start_level(self):
        self.field = Field()
        self.game_state = GameState.progress

    def handle_events(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
                if event.type == pygame.KEYDOWN and event.key == pygame.K_r:
                    self.start_level()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.click_field(event)

            self.draw()

            pygame.display.flip()
            pygame.time.wait(33)

    def click_field(self, event):
        if not self.cursor_in_field(*event.pos):
            return

        if self.game_state != GameState.progress:
            return

        if event.button == pygame.BUTTON_LEFT:
            self.open_cell(*event.pos)
        elif event.button == pygame.BUTTON_RIGHT:
            self.flag_cell(*event.pos)

        self.game_state = self.get_game_state()

        if self.game_state == GameState.win:
            self.flag_mines()

    def get_game_state(self):
        closed_cells = False
        for row in self.field.cells:
            for cell in row:
                if cell.has_mine and cell.is_open:
                    return GameState.lose
                if not cell.has_mine and not cell.is_open:
                    closed_cells = True

        if closed_cells:
            return GameState.progress

        return GameState.win

    def flag_mines(self):
        for row in self.field.cells:
            for cell in row:
                if cell.has_mine:
                    cell.has_flag = True

    def open_cell(self, cursor_x, cursor_y):
        if not self.cursor_in_field(cursor_x, cursor_y):
            return

        cell_row, cell_col = self.get_cell_by_pos(cursor_x, cursor_y)
        self.field.open_cell(cell_row, cell_col)

    def flag_cell(self, cursor_x, cursor_y):
        if not self.cursor_in_field(cursor_x, cursor_y):
            return

        cell_row, cell_col = self.get_cell_by_pos(cursor_x, cursor_y)
        self.field.toggle_flag(cell_row, cell_col)

    def draw(self):
        SCREEN.fill((74, 117, 44))
        self.draw_menu()
        self.draw_field()
        # TODO: вызовы методов draw_mines и draw_mine_counts нужны только для
        #  отладки. После завершения работы над приложением их нужно убрать.
        # self.draw_mines()
        # self.draw_mine_counts()

    def draw_menu(self):
        flags_set = self.count_flags()
        flags_left = self.field.mines_count - flags_set
        flag_counter = self.menu_font.render(
            str(flags_left), True, self.menu_font_color)

        self.draw_flag(430, 15)
        SCREEN.blit(flag_counter, (470, 20))
        if self.game_state == GameState.win:
            win_text = self.menu_font.render(
                'You win! Congratulations', True, self.menu_font_color)
            SCREEN.blit(win_text, (20, 20))
        elif self.game_state == GameState.lose:
            lose_text = self.menu_font.render(
                'You lose, press R to restart', True, self.menu_font_color)
            SCREEN.blit(lose_text, (20, 20))

    def count_flags(self):
        flags_count = 0
        for row in self.field.cells:
            for cell in row:
                if cell.has_flag:
                    flags_count += 1
        return flags_count

    def draw_field(self):
        for cell_row in range(len(self.field.cells)):
            for cell_col in range(len(self.field.cells[cell_row])):
                self.draw_cell(cell_row, cell_col)
        self.hover_cell()

    def draw_cell(self, cell_row, cell_col, hovered=False):
        cell_x = cell_col * CELL_SIDE
        cell_y = MENU_HEIGHT + cell_row * CELL_SIDE
        cell = self.field.cells[cell_row][cell_col]
        rect = pygame.Rect(cell_x, cell_y, CELL_SIDE, CELL_SIDE)
        if not cell.is_open:
            if hovered:
                color = self.selected_colors[0]
            else:
                color_type = self.get_cell_color_type(cell_row, cell_col)
                color = self.closed_colors[color_type]
            pygame.draw.rect(SCREEN, color, rect)
            if cell.has_flag:
                self.draw_flag(cell_x, cell_y)

        else:
            color_type = self.get_cell_color_type(cell_row, cell_col)
            color = self.open_colors[color_type]
            pygame.draw.rect(SCREEN, color, rect)
            if cell.has_mine:
                pygame.draw.rect(SCREEN, self.mined_cell_color, rect)
                self.draw_mine(cell_row, cell_col)
            else:
                self.draw_mine_count(cell_row, cell_col)

    def draw_flag(self, x, y):
        points = (
            (x + 10, y + 6),
            (x + CELL_SIDE - 6, y + CELL_SIDE // 2),
            (x + 10, y + CELL_SIDE - 6)
        )
        pygame.gfxdraw.aapolygon(SCREEN, points, self.flag_color)
        pygame.gfxdraw.filled_polygon(SCREEN, points, self.flag_color)

    def get_cell_color_type(self, cell_row, cell_col):
        return (
            (cell_row * WIDTH + cell_col + cell_row) % len(self.closed_colors))

    def hover_cell(self):
        cursor_x, cursor_y = pygame.mouse.get_pos()
        if (cursor_x < 0 or cursor_x > SCREEN_WIDTH
                or cursor_y < MENU_HEIGHT or cursor_y > SCREEN_HEIGHT):
            return

        cell_row, cell_col = self.get_cell_by_pos(cursor_x, cursor_y)
        self.draw_cell(cell_row, cell_col, hovered=True)

    def get_cell_by_pos(self, cursor_x, cursor_y):
        """
        :return: (row, col)
        """
        return (cursor_y - MENU_HEIGHT) // CELL_SIDE, cursor_x // CELL_SIDE

    def cursor_in_field(self, cursor_x, cursor_y):
        return (0 <= cursor_x <= SCREEN_WIDTH
                and MENU_HEIGHT <= cursor_y <= SCREEN_HEIGHT)

    def draw_mines(self):
        for cell_row in range(len(self.field.cells)):
            for cell_col in range(len(self.field.cells[cell_row])):
                if self.field.cells[cell_row][cell_col].has_mine:
                    self.draw_mine(cell_row, cell_col)

    def draw_mine(self, cell_row, cell_col):
        cell_x = cell_col * CELL_SIDE
        cell_y = MENU_HEIGHT + cell_row * CELL_SIDE
        rect = pygame.Rect(cell_x, cell_y, CELL_SIDE, CELL_SIDE)
        SCREEN.blit(self.mine_img, rect)

    def draw_mine_counts(self):
        for cell_row in range(len(self.field.cells)):
            for cell_col in range(len(self.field.cells[cell_row])):
                if not self.field.cells[cell_row][cell_col].has_mine:
                    self.draw_mine_count(cell_row, cell_col)

    def draw_mine_count(self, cell_row, cell_col):
        cell_x = cell_col * CELL_SIDE
        cell_y = MENU_HEIGHT + cell_row * CELL_SIDE
        cell = self.field.cells[cell_row][cell_col]
        if cell.near_mine_count == 0:
            return
        color = self.get_mine_count_color(cell.near_mine_count)
        text = self.menu_font.render(
            str(cell.near_mine_count), True, color)
        SCREEN.blit(text, (cell_x + 8, cell_y + 5))

    def get_mine_count_color(self, near_mine_count):
        color_number = (near_mine_count - 1) % len(self.mine_count_colors)
        return self.mine_count_colors[color_number]


class Field:
    def __init__(self):
        self.cells = []
        self.mines_count = 40

        for row in range(HEIGHT):
            cells_row = []
            for col in range(WIDTH):
                cells_row.append(Cell())
            self.cells.append(cells_row)

        self.place_mines()
        self.set_mine_count()

    def place_mines(self):
        # TODO(EP): оформить как задание
        mined_cells = list(range(WIDTH * HEIGHT))
        random.shuffle(mined_cells)
        for cell_number in mined_cells[:self.mines_count]:
            cell_row = cell_number // WIDTH
            cell_coll = cell_number % WIDTH
            self.cells[cell_row][cell_coll].has_mine = True

    def set_mine_count(self):
        # TODO(EP): Оформить как задание
        for cell_row in range(len(self.cells)):
            for cell_col in range(len(self.cells[cell_row])):
                near_cells = self.get_near_cells(cell_row, cell_col)
                mine_count = self.count_mines(near_cells)
                self.cells[cell_row][cell_col].near_mine_count = mine_count

    def get_near_cells(self, cell_row, cell_col):
        cells_coords = self.get_near_cells_coords(cell_row, cell_col)
        cells = []
        for cell_row, cell_col in cells_coords:
            cells.append(self.cells[cell_row][cell_col])
        return cells

    def get_near_cells_coords(self, cell_row, cell_col):
        cells_coords = (
            self.maybe_get_cell(cell_row - 1, cell_col - 1),
            self.maybe_get_cell(cell_row - 1, cell_col),
            self.maybe_get_cell(cell_row - 1, cell_col + 1),
            self.maybe_get_cell(cell_row, cell_col + 1),
            self.maybe_get_cell(cell_row + 1, cell_col + 1),
            self.maybe_get_cell(cell_row + 1, cell_col),
            self.maybe_get_cell(cell_row + 1, cell_col - 1),
            self.maybe_get_cell(cell_row, cell_col - 1)
        )
        return list(filter(None, cells_coords))

    def maybe_get_cell(self, cell_row, cell_col):
        if (cell_row < 0 or cell_row >= len(self.cells)
                or cell_col < 0 or cell_col >= len(self.cells[0])):
            return None
        return (cell_row, cell_col)

    def count_mines(self, cells):
        mines = 0
        for cell in cells:
            if cell.has_mine:
                mines += 1
        return mines

    def open_cell(self, cell_row, cell_col):
        """
        Открывает закрытую ячейку, если на ней нет флага.
        """
        # TODO(EP): оформить как задание
        cell = self.cells[cell_row][cell_col]
        if cell.is_open or cell.has_flag:
            return

        cell.is_open = True

        if cell.has_mine:
            return

        if cell.near_mine_count > 0:
            cell.is_open = True
        elif cell.near_mine_count == 0:
            near_cells = self.get_near_cells_coords(cell_row, cell_col)
            for cell_row, cell_col in near_cells:
                self.open_cell(cell_row, cell_col)

        return

    def toggle_flag(self, cell_row, cell_col):
        """
        Устанавливает флаг на закрытой ячейке.
        """
        cell = self.cells[cell_row][cell_col]
        if not cell.is_open:
            cell.has_flag = not cell.has_flag


class Cell:
    def __init__(self):
        self.is_open = False
        self.has_flag = False
        self.has_mine = False
        self.near_mine_count = 0


ui = Ui()
ui.start_level()
ui.handle_events()
