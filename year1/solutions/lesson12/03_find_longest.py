def find_longest(lst):
    if len(lst) == 0:
        return None

    longest = ""
    max_length = 0

    for item in lst:
        if len(item) > max_length:
            max_length = len(item)
            longest = item

    return longest


assert find_longest([]) == None
assert find_longest([""]) == ""
assert find_longest(["42"]) == "42"
assert find_longest(["uno", "dos", "tre"]) == "uno"
assert find_longest(["a", "bb", "ccc"]) == "ccc"
assert find_longest(["a", "ccc", "bb"]) == "ccc"
