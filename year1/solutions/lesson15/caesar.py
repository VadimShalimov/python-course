import easygui


def encode_symbol(symbol, key):
    size = ord('z') - ord('a')
    if 'a' <= symbol <= 'z':
        return chr(((ord(symbol) - ord('a') + key) % size) + ord('a'))
    elif 'A' <= symbol <= 'Z':
        return chr(((ord(symbol) - ord('A') + key) % size) + ord('A'))
    return symbol


def encode_str(string, key):
    result = ""
    for symbol in string:
        result += encode_symbol(symbol, key)
    return result


def encode_file(input_file_path, output_file_path, key):
    input_file = open(input_file_path)
    output_file = open(output_file_path, 'w')

    for line in input_file:
        encoded_line = encode_str(line, key)
        output_file.write(encoded_line)

    input_file.close()
    output_file.close()


# encode_file('caesar.py', 'encoded.py', 3)
# encode_file('encoded.py', 'decoded.py', -3)
# assert encode_str('Hello, World!', 3) == 'Khoor, Aruog!'
# assert encode_str('Khoor, Aruog!', -3) == 'Hello, World!'


def encryption_app():
    input_file_path = easygui.fileopenbox('File to encrypt')
    output_file_path = easygui.filesavebox('Where to save?')
    key = easygui.integerbox('Shift')
    encode_file(input_file_path, output_file_path, key)


encryption_app()
